/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.iigc.hadithfilegenerator.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/** @author georg */
public final class HadithList {

  private final ObservableList<Hadith> olHadithList;
  private final SimpleListProperty<Hadith> hadithListProperty;
  private final ObjectMapper om;
  
  public HadithList() {

      this.om = new ObjectMapper();
    olHadithList = FXCollections.observableArrayList();
    hadithListProperty = new SimpleListProperty<>(olHadithList);
  }

  public SimpleListProperty returnListProperty() {

    return hadithListProperty;
  }

  public ObservableList<Hadith> returnList() {
    return olHadithList;
  }

  public void fromFileToList(File fileJSON) throws IOException {

    olHadithList.addAll(Arrays.asList(om.readValue(fileJSON, Hadith[].class)));
  }

  public void fromListToFile(File fileJSON) throws FileNotFoundException, IOException {

    om.writeValue(new FileOutputStream(fileJSON), olHadithList);
  }

  @Override
  public String toString() {

    String jsonData = null;
    try {

      jsonData = om.writeValueAsString(olHadithList);

    } catch (JsonProcessingException ex) {
    }

    return jsonData;
  }
}
