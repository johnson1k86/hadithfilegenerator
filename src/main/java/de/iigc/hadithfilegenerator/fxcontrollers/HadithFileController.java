/*
 * Copyright (C) 2020 georg
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.iigc.hadithfilegenerator.fxcontrollers;

import de.iigc.hadithfilegenerator.model.Hadith;
import de.iigc.hadithfilegenerator.model.HadithList;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextArea;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class HadithFileController implements Initializable {

    private File fileHadith;
    private final HadithList hadithList;

    @FXML
    MenuItem menuOpen;

    @FXML
    MenuItem menuSave;

    @FXML
    MenuItem menuClose;

    @FXML
    ListView listViewHadith;

    @FXML
    TextArea textArabic;

    @FXML
    TextArea textLatin;

    @FXML
    Button buttonNew;

    @FXML
    Button buttonRemove;


    public HadithFileController() {

        hadithList = new HadithList();
        listViewHadith = new ListView<>();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        listViewHadith.itemsProperty().bindBidirectional(hadithList.returnListProperty());
        listViewHadith.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        listViewHadith.setCellFactory(
                (p) -> {
                    ListCell<Hadith> cell
                    = new ListCell<>() {

                @Override
                protected void updateItem(Hadith t, boolean empty) {
                    super.updateItem(t, empty);
                    if (empty || t == null) {
                        setText(null);
                    } else {
                        setText(Integer.toString(super.getIndex() + 1));
                    }
                }
            };
                    return cell;
                });
        listViewHadith
                .getSelectionModel()
                .selectedItemProperty()
                .addListener(
                        (ov, oldValue, newValue) -> {
                            if (oldValue != null) {
                                textArabic.textProperty().unbindBidirectional(((Hadith) oldValue).arabicProperty());
                                textLatin.textProperty().unbindBidirectional(((Hadith) oldValue).latinProperty());
                                textArabic.clear();
                                textLatin.clear();
                            }

                            if (newValue == null) {
                                textArabic.clear();
                                textLatin.clear();
                                textArabic.setDisable(true);
                                textLatin.setDisable(true);
                            } else {
                                textArabic.textProperty().bindBidirectional(((Hadith) newValue).arabicProperty());
                                textLatin.textProperty().bindBidirectional(((Hadith) newValue).latinProperty());
                                textArabic.setDisable(false);
                                textLatin.setDisable(false);
                            }

                        });
    }

    @FXML
    public void newHadith() {

        Hadith hadith = new Hadith();
        hadithList.returnList().add(hadith);
    }

    @FXML
    public void removeHadith() {

        hadithList.returnListProperty().remove(listViewHadith.getSelectionModel().getSelectedItem());
    }

    @FXML
    private void openFile() {

        FileChooser fc = new FileChooser();
        fc.setTitle("JSON Datei öffnen");
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("JSON-Dateien", "*.json"));
        fileHadith = fc.showOpenDialog(new Stage());
        try {
            hadithList.fromFileToList(fileHadith);
        } catch (IOException ex) {
            
        }
    }

    @FXML
    private void saveFile() {

        FileChooser fc = new FileChooser();
        fc.setTitle("JSON Datei speichern");
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("JSON-Dateien", "*.json"));
        fileHadith = fc.showSaveDialog(new Stage());
        try {
            hadithList.fromListToFile(fileHadith);
        } catch (IOException ex) {

        }
    }

    @FXML
    public void showAbout() {

        Alert about = new Alert(Alert.AlertType.INFORMATION);
        about.setTitle("Über");
        about.setHeaderText("Hadithinator 3000");
        about.setContentText("Brought to you by iigc.de");
        about.showAndWait();
    }

    @FXML
    private void closeApp() {

        Platform.exit();
    }
}
