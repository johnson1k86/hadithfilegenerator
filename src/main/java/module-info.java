/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

module HadithFileGenerator {
    requires com.fasterxml.jackson.databind;
    requires com.fasterxml.jackson.annotation;
    requires com.fasterxml.jackson.core;
    requires javafx.base;
    requires javafx.controls;
    requires javafx.graphics;
    requires javafx.fxml;
    exports de.iigc.hadithfilegenerator;
    exports de.iigc.hadithfilegenerator.fxcontrollers;
    exports de.iigc.hadithfilegenerator.model;
    opens de.iigc.hadithfilegenerator.fxcontrollers to javafx.fxml;
}